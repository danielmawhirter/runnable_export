#!/usr/bin/env python

import numpy as np
import tensorflow as tf
from tensorflow.python.framework import tensor_util
import os, json

dtype_map = { "float32": "DT_FLOAT", "float64": "DT_DOUBLE", "int32": "DT_INT", "int64": "DT_LONG" }
required_attrs = { 'Split': [ 'num_split' ], 'Squeeze': [ 'squeeze_dims' ],
                   'Unpack': [ 'axis' ], 'Pack': [ 'axis' ], 'Cast': [ 'DstT' ],
                   'Conv2D': [ 'strides', 'padding' ],
                   'MaxPool': [ 'ksize', 'strides', 'padding' ],
                   'AvgPool': [ 'ksize', 'strides', 'padding' ] }

def tensor_to_string(tensor, separator=' '):
  tokens = []
  if str(tensor.dtype) in dtype_map:
    tokens.append(dtype_map[str(tensor.dtype)])
  else:
    tokens.append('DT_NONE')
  tokens.append(str(len(tensor.shape)))
  for dim in tensor.shape:
    tokens.append(str(dim))
  for e in np.nditer(tensor, order='C'):
    tokens.append(str(e))
  return separator.join(tokens)

def attr_to_const(obj):
  if obj.HasField('i'):
    return "DT_INT 0 " + str(obj.i)
  elif obj.HasField('s'):
    return "DT_STRING 0 " + json.dumps(obj.s.decode("utf-8"))
  elif obj.HasField('type'):
    if obj.type in type_arr:
      return "DT_STRING 0 " + json.dumps(type_arr[obj.type])
    print("New type: " + str(obj).strip() + " == " + str(obj.type))
  elif obj.HasField('list'):
    ints = []
    for v in obj.list.i:
      ints.append(str(v))
    if len(ints) > 0:
      return "DT_INT 1 " + str(len(ints)) + " " + " ".join(ints)
  return "DT_NONE (extract manually)"

def write_tensor(fname, arr):
  try:
    os.makedirs('./' + '/'.join(fname.split('/')[:-1]))
  except OSError as e:
    pass
  print("writing", fname)
  with open(fname, 'w') as outf:
    outf.write(tensor_to_string(arr, '\n'))
  
def op_name(name):
  split = name.split(':')
  split.append('0')
  return (split[0], str(split[1]))

def var_name(name):
  if name.endswith('/read'):
    return name[:-5]
  else:
    return name.split(':')[0]

def export_subgraph(export_prefix, sess, output_tensors, feed_dict):
  assert isinstance(output_tensors, list)
  assert isinstance(feed_dict, dict)
  try:
    os.makedirs('./' + '/'.join(export_prefix.split('/')[:-1]))
  except OSError as e:
    pass
  graph_def = sess.graph.as_graph_def()
  nodes = [ n for n in graph_def.node ]
  op_lookup = dict([ ( op_name(nodes[i].name)[0], i ) for i in range(len(nodes)) ])
  active = set([ op_lookup[op_name(t.name)[0]] for t in output_tensors ])
  available_inputs = ( set([ op_lookup[op_name(n)[0]] for n in feed_dict ]) |
                        set([ i for i in range(len(nodes)) if nodes[i].op in
                                ['Placeholder', 'Const', 'ReadVariableOp'] or
                          ( nodes[i].op == 'Identity' and nodes[i].name.endswith('/read') ) ]) )
  graph_nodes = active.copy()

  # select graph nodes
  while len(active) > 0:
    new_active = set()
    for i in active:
      for req in nodes[i].input:
        if not op_name(req)[0] in op_lookup:
          print("no nodes found with name: " + req)
          continue
        idx = op_lookup[op_name(req)[0]]
        if not idx in graph_nodes:
          graph_nodes.add(idx)
          if not idx in available_inputs:
            new_active.add(idx)
    active = new_active.copy()
  print("found", len(graph_nodes), "graph nodes")
  #print('\n'.join([ str((nodes[i].op, op_name(nodes[i].name)[0])) for i in graph_nodes ]))

  # attributes as constants
  attr_consts = {}
  for i in sorted(graph_nodes):
    if nodes[i].op in required_attrs:
      attr_consts[nodes[i].name] = [ ( nodes[i].name + '/' + required_attrs[nodes[i].op][idx],
                                   attr_to_const(nodes[i].attr[required_attrs[nodes[i].op][idx]]),
                                   idx + len(nodes[i].input) )
                                 for idx in range(len(required_attrs[nodes[i].op])) ]

  # output graph
  with open(export_prefix + '.def', 'w') as f:
    # inputs
    f.write('INPUT\n')
    input_indices = set([ op_lookup[op_name(x)[0]] for x in feed_dict.keys() ])
    print(feed_dict.keys())
    for i in sorted(graph_nodes & input_indices):
      vname = var_name(nodes[i].name)
      f.write(vname + '\n')
      print('running: ' + vname)
      write_tensor(export_prefix + "_vars/" + vname + ".var", sess.run(nodes[i].name + ':0', feed_dict))

    f.write('CONST\n')
    const_indices = available_inputs - input_indices
    for i in sorted(graph_nodes & const_indices):
      vname = var_name(nodes[i].name)
      arr = tensor_util.MakeNdarray(nodes[i].attr["value"].tensor) if nodes[i].op == 'Const' else sess.run(nodes[i].name + ':0', feed_dict)
      f.write(vname + ' ' + tensor_to_string(arr) + '\n')
    for n in attr_consts:
      for t in attr_consts[n]:
        f.write(t[0] + ' ' + t[1] + '\n')

    f.write('OP\n')
    for i in sorted(graph_nodes):
      if i not in available_inputs and nodes[i].op != 'Const':
        f.write(nodes[i].op + ' ' + nodes[i].name + '\n')

    f.write('INPUT_FEED\n')
    for i in sorted(graph_nodes):
      if i not in available_inputs:
        for k,req in zip(range(len(nodes[i].input)), nodes[i].input):
          req_name = op_name(req)[0]
          if not req_name in op_lookup:
            continue
          idx = op_lookup[req_name]
          if idx in available_inputs or nodes[i].op == 'Const':
            f.write(nodes[i].name + ' ' + str(k) + ' ' + var_name(nodes[idx].name) + '\n')
    for n in attr_consts:
      for t in attr_consts[n]:
        f.write(n + ' ' + str(t[2]) + ' ' + t[0] + '\n')

    f.write('EDGE\n')
    for i in sorted(graph_nodes):
      if i not in available_inputs:
        for k,req in zip(range(len(nodes[i].input)), nodes[i].input):
          if op_lookup[op_name(req)[0]] not in available_inputs and nodes[op_lookup[op_name(req)[0]]].op != 'Const':
            f.write(nodes[i].name + ' ' + str(k) + ' ' + op_name(req)[0] + ' ' + str(op_name(req)[1]) + '\n')

  for i in sorted(graph_nodes):
    idx = 0
    success = True
    while success:
      tname = nodes[i].name + ':' + str(idx)
      try:
        arr = sess.run(tname, feed_dict)
        write_tensor(export_prefix + '_canon/' + tname, arr)
        idx += 1
      except:
        success = False

